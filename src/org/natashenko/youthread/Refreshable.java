package org.natashenko.youthread;

public interface Refreshable {
    void onRefreshStart();
    void onRefreshComplete();
}
