package org.natashenko.youthread.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.natashenko.youthread.Utils;

import android.text.format.DateUtils;
import android.util.Log;

public class DateConverter {
	
	/**
	 * Convert time in milliseconds to date in format YYYY-MM-DDTHH:MM:SS
	 * @param time
	 * @return string value of date
	 */
	public static String toYouTrackDate(long time) {
		String youTrackDate = new String();
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
			youTrackDate = formatter.format(new Date(time));
		} catch (Exception e) {
			Log.e("log", e.getClass().getName() + ": " + e.getMessage());
		}
		return youTrackDate;
	}
	
	public static String toNiceDate(long time) {
		return DateUtils.formatDateTime(Utils.CONTEXT.getApplicationContext(),
		    time, DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME | 
		    DateUtils.FORMAT_ABBREV_ALL | DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_WEEKDAY);
	}
	
	
}
