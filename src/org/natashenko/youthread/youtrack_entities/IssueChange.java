package org.natashenko.youthread.youtrack_entities;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.natashenko.youthread.R;
import org.natashenko.youthread.Utils;

public class IssueChange {
	private String issueId;
	private List<FieldChange> fieldChanges = new LinkedList<FieldChange>();
	private long updated;
	private String updaterName;
	
	public long getUpdated() {
		return updated;
	}
	
	public void setUpdated(long time) {
		updated = time;
	}
	
	public String getUpdaterName() {
		return updaterName;
	}
	
	public void setUpdaterName(String name) {
		updaterName = name;
	}
	
	public String toString() {
		StringBuilder fieldChangesSting = new StringBuilder();
		for (FieldChange change : fieldChanges) {
			fieldChangesSting.append("\n" + change);
		}
		return String.format(Utils.getResourse(R.string.issue_change_to_string), 
			issueId, new Date(updated), updaterName, fieldChangesSting);
	}
	
	private class FieldChange {
		private String fieldName;
		private String newValue;
		private String oldValue;
		
		public String getFieldName() {
			return fieldName;
		}
		public void setFieldName(String fieldName) {
			this.fieldName = fieldName;
		}
		public String getNewValue() {
			return newValue;
		}
		public void setNewValue(String newValue) {
			this.newValue = newValue;
		}
		public String getOldValue() {
			return oldValue;
		}
		public void setOldValue(String oldValue) {
			this.oldValue = oldValue;
		}
		
		public String toString() {
			return String.format(Utils.getResourse(R.string.field_change_to_string), fieldName,
				newValue, oldValue);
		}
	}
}
