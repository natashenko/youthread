package org.natashenko.youthread.service;

import org.apache.http.HttpStatus;
import org.natashenko.youthread.LoginActivity;
import org.natashenko.youthread.Result;
import org.w3c.dom.Element;

import android.widget.Toast;

public class LoginRequest extends RESTRequest<LoginActivity> {
    
    public LoginRequest(LoginActivity activity) {
        super(activity, RESTService.HttpVerb.POST, "rest/user/login");
    }

    @Override
    public void form() {
        setHost(activity.getHost());
        setParam("login", activity.getUser());
        setParam("password", activity.getPassword());
    }
    
    @Override
    public void onOKResult(Element rootResultElement) {
        // TODO save host and login
        // repeat old request
        // if (nextRequest != null) {
        // nextRequest.execute();
        // }
        activity.setResult(Result.OK);
        activity.finish();
    }

    @Override
    public void onErrorResult(int code, Element rootResultElement) {
        if (code == HttpStatus.SC_FORBIDDEN) {
            activity.setPasswordError();
        }
    }

    @Override
    public void onException(String type, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }
}
