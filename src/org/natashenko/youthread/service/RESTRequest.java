package org.natashenko.youthread.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpStatus;
import org.apache.http.message.BasicNameValuePair;
import org.natashenko.youthread.Executable;
import org.natashenko.youthread.Refreshable;
import org.natashenko.youthread.Resultable;
import org.natashenko.youthread.Utils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;

public abstract class RESTRequest <T extends Activity & Refreshable > implements Resultable, Executable {
	public static int MAX_ITEMS_COUNT = 10;

	protected T activity;
	private String host;
	private String shortUri;
	private RESTService.HttpVerb verb;
	private List<BasicNameValuePair> params;

	public RESTRequest(T activity, RESTService.HttpVerb verb, String shortUri) {
		this.activity = activity;
		this.verb = verb;
		this.shortUri = shortUri;
		this.params = new ArrayList<BasicNameValuePair>();
	}

	public void setParam(String name, String value) {
		params.add(new BasicNameValuePair(name, value));
	}

	private Uri formRequestUri() {
		Uri uri = Uri.parse(getHost() + shortUri);
		if (!params.isEmpty()) {
			Uri.Builder uriBuilder = uri.buildUpon();
			for (BasicNameValuePair param : params) {
				uriBuilder.appendQueryParameter(param.getName(),
						param.getValue());
			}
			uri = uriBuilder.build();
		}
		return uri;
	}
	
	private String getHost() {
	    if (TextUtils.isEmpty(host)) {
	        return Utils.getHost();
	    } else {
	        return host;
	    }
	}
	
	public void setHost(String host) {
	    this.host = host;
	}

	@Override
	public void execute() {
	    activity.onRefreshStart();
	    form();
		ResultReceiver resultReceiver = new ResultReceiver(new Handler()) {
			@Override
			protected void onReceiveResult(int resultCode, Bundle resultData) {
			    activity.onRefreshComplete();
				try {
					if (resultCode == RESTService.EXCEPTION_RESULT_CODE) {
						String type = resultData.getString(RESTService.EXCEPTION_TYPE);
						String message = resultData.getString(RESTService.EXCEPTION_MESSAGE);
						Log.e(Utils.LOG_TAG, type + ": " + message);
						onException(type, message);
					} else {
						String result = resultData.getString(RESTService.RESULT_BODY);
						Element rootResultElement = parse(result);
//						Log.i(Utils.LOG_TAG, resultCode + " " + result);
						if (resultCode == HttpStatus.SC_OK) {
							onOKResult(rootResultElement);
						} else {
							onErrorResult(resultCode, rootResultElement);
						}
					}
				} catch (Exception e) {
					Log.e(Utils.LOG_TAG, getClass().getSimpleName() + ".execute() " + e.getMessage());
				}
			}
		};
		Intent intent = new Intent(activity, RESTService.class);
		Uri uri = formRequestUri();
		intent.setData(uri);
		intent.putExtra(RESTService.EXTRA_HTTP_VERB, verb);
		intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, resultReceiver);
		activity.startService(intent);
	}
	
	private static Element parse(String text)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputStream stream = new ByteArrayInputStream(text.getBytes());
		Document dom = builder.parse(stream);
		Element root = dom.getDocumentElement();
		return root;
	}
	
}
