package org.natashenko.youthread;

public interface Executable {
    void execute();
}
