package org.natashenko.youthread;

import org.w3c.dom.Element;

public interface Resultable {
    void form();
	void onOKResult(Element rootResultElement);
	void onErrorResult(int code, Element rootResultElement);
	void onException(String type, String message);
}
