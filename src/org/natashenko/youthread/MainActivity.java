package org.natashenko.youthread;

import java.util.LinkedList;

import org.natashenko.youthread.service.GetNewerIssuesRequest;
import org.natashenko.youthread.service.GetOlderIssuesRequest;
import org.natashenko.youthread.youtrack_entities.Issue;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshBase.State;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.extras.SoundPullEventListener;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class MainActivity extends ListActivity implements Refreshable {
    private static LinkedList<Issue> items = new LinkedList<Issue>();

    private ArrayAdapter<Issue> arrayAdapter; // contains issues list
    private PullToRefreshListView pullRefreshListView;
    private Executable prevRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Utils.LOG_TAG, getClass().getSimpleName() + ": onCreate");
        Utils.CONTEXT = this;
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_main);
        // set up pull to refresh view from Chris Banes library
        // (located in https://github.com/chrisbanes/Android-PullToRefresh)
        pullRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);
        pullRefreshListView.setMode(Mode.BOTH);
        pullRefreshListView.setOnRefreshListener(new OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                pullRefreshListView.setLastUpdatedLabel(DateUtils.formatDateTime(getApplicationContext(),
                        System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE));
                downloadNewerItems();

            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                downloadOlderItems();
            }
        });
        // set sound when user refresh data
        SoundPullEventListener<ListView> soundListener = new SoundPullEventListener<ListView>(this);
        soundListener.addSoundEvent(State.PULL_TO_REFRESH, R.raw.pull_event);
        soundListener.addSoundEvent(State.RESET, R.raw.reset_sound);
        soundListener.addSoundEvent(State.REFRESHING, R.raw.refreshing_sound);
        pullRefreshListView.setOnPullEventListener(soundListener);
        // set data to the adapter
        arrayAdapter = new ArrayAdapter<Issue>(this, android.R.layout.simple_list_item_1, items);
        setListAdapter(arrayAdapter);
    }

    @Override
    protected void onPause() {
        Log.i(Utils.LOG_TAG, getClass().getSimpleName() + ": onPause");
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.i(Utils.LOG_TAG, getClass().getSimpleName() + ": onResume");
        if (items.isEmpty()) {
            downloadNewerItems();
        }
        super.onResume();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.i(Utils.LOG_TAG, getClass().getSimpleName() + ": onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.i(Utils.LOG_TAG, getClass().getSimpleName() + ": onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    public void refreshAdapter() {
        arrayAdapter.notifyDataSetChanged();
    }

    private void downloadNewerItems() {
        GetNewerIssuesRequest request = new GetNewerIssuesRequest(this);
        request.execute();
        prevRequest = request;
    }

    private void downloadOlderItems() {
        GetOlderIssuesRequest request = new GetOlderIssuesRequest(this);
        request.execute();
        prevRequest = request;
    }

    @Override
    public void onRefreshStart() {
        setProgressBarIndeterminateVisibility(true);
    }

    public void onRefreshComplete() {
        setProgressBarIndeterminateVisibility(false);
        pullRefreshListView.onRefreshComplete();
    }

    public LinkedList<Issue> getViewedItems() {
        return items;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case RequestCode.LOGIN_ACTIVITY: {
            if (resultCode == Result.OK && prevRequest != null) {
                Log.i(Utils.LOG_TAG, "exec prev request");
                prevRequest.execute();
            }
            Log.i(Utils.LOG_TAG, getLocalClassName() + ": onActivityResult() " + "  result " + resultCode);
            break;
        }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
