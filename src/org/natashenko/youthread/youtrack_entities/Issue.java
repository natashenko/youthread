package org.natashenko.youthread.youtrack_entities;

import org.natashenko.youthread.R;
import org.natashenko.youthread.Utils;
import org.natashenko.youthread.service.DateConverter;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Issue implements Comparable<Issue> {
	private String id;
	private String summary;
	private long updated;
	private String updatedAsYouTrackString;
	private String updaterName;
	
	public static Issue parse(Node root) {
		Issue issue = new Issue();
		String idAttr = Utils.getResourse(R.string.attr_id);
		String idValue = root.getAttributes().getNamedItem(idAttr).getNodeValue();
		issue.setId(idValue);
		NodeList issueSubTags = root.getChildNodes();
		for (int i = 0; i < issueSubTags.getLength(); ++i) {
			Node issueSubTag = issueSubTags.item(i);
			String tag_field = Utils.getResourse(R.string.tag_field);
			if (issueSubTag.getNodeName().equals(tag_field)) {
				String nameAttr = Utils.getResourse(R.string.attr_name);
				String nameValue = issueSubTag.getAttributes().getNamedItem(nameAttr).getNodeValue();
				String value = issueSubTag.getFirstChild().getTextContent();
				if (nameValue.equals(Utils.getResourse(R.string.attr_updated))) {
					issue.setUpdated(Long.parseLong(value));
				} else if (nameValue.equals(Utils.getResourse(R.string.attr_updaterName))) {
					issue.setUpdaterName(value);
				} else if (nameValue.equals(Utils.getResourse(R.string.attr_summary))) {
				    issue.setSummary(value);
				}
			}
		}
		return issue;
	}
	
	public String getId() {
		return id;
	}
	
	private void setId(String id) {
		this.id = id;
	}
	
	public long getUpdated() {
		return updated;
	}
	
	public void setUpdated(long updated) {
		this.updated = updated;
		this.updatedAsYouTrackString = DateConverter.toYouTrackDate(updated);
	}
	
	public String getUpdaterName() {
		return updaterName;
	}
	
	private void setUpdaterName(String updaterName) {
		this.updaterName = updaterName;
	}
	
	public String getSummary() {
	    return summary;
	}
	
	public void setSummary(String summary) {
	    this.summary = summary;
	}
	
	public String toString() {
//	    return id + "   " + DateConverter.toNiceDate(updated) + "  " + updated;
		return String.format(Utils.getResourse(R.string.issue_to_string),
		    updaterName, id, summary, DateConverter.toNiceDate(updated));
	}
	
	public String getUpdatedAsYouTrackString() {
	    return updatedAsYouTrackString;
	}

	@Override
	public int compareTo(Issue another) {
		if (this.getUpdated() == another.getUpdated()) {
			return 0;
		} else if (this.getUpdated() < another.getUpdated()) {
			return 1;
		} else {
			return -1;
		}
	}

}

