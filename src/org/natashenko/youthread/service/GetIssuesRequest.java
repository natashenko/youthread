package org.natashenko.youthread.service;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;
import org.natashenko.youthread.LoginActivity;
import org.natashenko.youthread.MainActivity;
import org.natashenko.youthread.R;
import org.natashenko.youthread.RequestCode;
import org.natashenko.youthread.Utils;
import org.natashenko.youthread.youtrack_entities.Issue;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public abstract class GetIssuesRequest extends RESTRequest<MainActivity> {
    
	public GetIssuesRequest(MainActivity activity) {
		super(activity, RESTService.HttpVerb.GET, "rest/issue");
	}
	
	@Override
	public void form() {
		setParam("with", Utils.getResourse(R.string.attr_updated));
		setParam("with", Utils.getResourse(R.string.attr_updaterName));
		setParam("with", Utils.getResourse(R.string.attr_summary));
		setParam("max", String.valueOf(MAX_ITEMS_COUNT));
		if (activity.getViewedItems().isEmpty()) {
		    setParam("filter", "updated: today sort by: updated desc");
		} else {
		    Issue extremeIssue = getExtremeItem();
		    String updated = extremeIssue.getUpdatedAsYouTrackString();
		    String filter = getFilterUpdatedValue(updated);
		    setParam("filter", filter);
		    int leaveOutCount = getViewedItemsCountWithSameDate(updated);
		    setParam("after", String.valueOf(leaveOutCount));
		}
	}
	
    @Override
    public void onOKResult(Element rootResultElement) {
        List<Issue> resultItems = getResultItems(rootResultElement);
        addResultToActivity(resultItems);
    }
    
    @Override
    public void onErrorResult(int code, Element rootResultElement) {
        if (code == HttpStatus.SC_UNAUTHORIZED) {
            String errorMessage = activity.getString(R.string.error_unauthorized);
            Toast.makeText(activity, errorMessage, Toast.LENGTH_SHORT).show();
            startLoginActivity();
            // activity.performLogin(this);
        }
    }
    
    @Override
    public void onException(String type, String message) {
        if (type == UnknownHostException.class.getSimpleName()) {
            startLoginActivity();
        } else {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
        }
    }
    
    private void startLoginActivity() {
        Intent intent = new Intent(activity, LoginActivity.class);
//        activity.startActivity(intent);
        activity.startActivityForResult(intent, RequestCode.LOGIN_ACTIVITY);
    }

    private List<Issue> getResultItems(Element root) {
        List<Issue> items = new ArrayList<Issue>();
        String issueTagName = Utils.getResourse(R.string.tag_issue);
        NodeList issuesList = root.getElementsByTagName(issueTagName);
        for (int i = 0; i < issuesList.getLength(); ++i) {
            Node issueNode = issuesList.item(i);
            Issue issue = Issue.parse(issueNode);
            items.add(issue);
        }
        return items;
    }
    
    private int getViewedItemsCountWithSameDate(String updated) {
        int count = 0;
        for (Issue issue : activity.getViewedItems()) {
            if (issue.getUpdatedAsYouTrackString().equals(updated)) {
                Log.d(Utils.LOG_TAG, "has same date: " + issue.getId());
                count++;
            }
        }
        return count;
    }
    
    protected abstract void addResultToActivity(List<Issue> resultItems);
    protected abstract String getFilterUpdatedValue(String date);
    protected abstract Issue getExtremeItem();
}
