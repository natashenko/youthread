package org.natashenko.youthread.service;

import java.util.LinkedList;
import java.util.List;

import org.natashenko.youthread.MainActivity;
import org.natashenko.youthread.Utils;
import org.natashenko.youthread.youtrack_entities.Issue;

import android.util.Log;

public class GetOlderIssuesRequest extends GetIssuesRequest {

    public GetOlderIssuesRequest(MainActivity activity) {
        super(activity);
    }

    @Override
    protected void addResultToActivity(List<Issue> resultItems) {
        LinkedList<Issue> activityItems = activity.getViewedItems();
        activityItems.addAll(resultItems);
//      Collections.sort(getList());
      activity.refreshAdapter();
      
      StringBuilder result = new StringBuilder();
      for (Issue issue : resultItems) {
          result.append(issue.getId() + " ");
      }
      Log.e(Utils.LOG_TAG, "result:" + result);
    }

    @Override
    protected String getFilterUpdatedValue(String date) {
        return "updated: 1970-01-01 .. " + date + " sort by: updated desc";
    }

    @Override
    protected Issue getExtremeItem() {
        return activity.getViewedItems().getLast();
    }

}
