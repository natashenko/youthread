package org.natashenko.youthread.service;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.natashenko.youthread.Utils;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class RESTService extends IntentService {

	private static final String TAG = RESTService.class.getName();
	
	private static BasicCookieStore cookieStore = new BasicCookieStore();
	
	public static final String EXTRA_HTTP_VERB = "org.natashenko.youthread.EXTRA_HTTP_VERB";
	public static final String EXTRA_PARAMS = "org.natashenko.youthread.EXTRA_PARAMS";
	public static final String EXTRA_RESULT_RECEIVER = "org.natashenko.youthread.EXTRA_RESULT_RECEIVER";
	public static final String RESULT_BODY = "org.natashenko.youthread.RESULT_BODY";
	public static final int EXCEPTION_RESULT_CODE = -1;
	public static final String EXCEPTION_TYPE = "org.natashenko.youthread.EXCEPTION_TYPE";
	public static final String EXCEPTION_MESSAGE = "org.natashenko.youthread.EXCEPTION_MESSAGE";

	public enum HttpVerb {
		GET, POST, PUT, DELETE
	}
	
	public RESTService() {
		super(TAG);
	}
	
	@Override
	public void onCreate() {
//		Log.i("log " + getClass().getSimpleName(), "onCreate");
		super.onCreate();
	}
	
	@Override
	public void onDestroy() {
//		Log.i("log " + getClass().getSimpleName(), "onDestroy");
		super.onDestroy();
	}
	
	@Override
	protected void onHandleIntent(Intent intent) {
		Uri uri = intent.getData();
		Bundle extras = intent.getExtras();
		if (extras == null || uri == null || !extras.containsKey(EXTRA_RESULT_RECEIVER)) {
			Log.e(Utils.LOG_TAG, "You did not pass extras or data with the Intent.");
			return;
		}
		HttpVerb verb = (HttpVerb) extras.get(EXTRA_HTTP_VERB);
		ResultReceiver receiver = extras.getParcelable(EXTRA_RESULT_RECEIVER);
		try {
			HttpRequestBase request = createRequest(uri, verb);
			performRequest(request, receiver);
		} catch (Exception e) {
			Bundle exception = new Bundle();
			exception.putString(EXCEPTION_TYPE, e.getClass().getSimpleName());
			exception.putString(EXCEPTION_MESSAGE, e.getMessage());
			receiver.send(EXCEPTION_RESULT_CODE, exception);
		}
	}

	private HttpRequestBase createRequest(Uri uri, HttpVerb verb) {
		switch (verb) {
		case POST:
			Log.d(Utils.LOG_TAG, "POST: " + uri.toString());
			return new HttpPost(uri.toString());
		case PUT:
			Log.d(Utils.LOG_TAG, "PUT: " + uri.toString());
			return new HttpPut(uri.toString());
		case DELETE: 
			Log.d(Utils.LOG_TAG, "DELETE: " + uri.toString());
			return new HttpDelete(uri.toString());
		default: 
			Log.d(Utils.LOG_TAG, "GET: " + uri.toString());
			return new HttpGet(uri.toString());
		}
	}

	private void performRequest(HttpRequestBase request, ResultReceiver receiver) 
			throws ClientProtocolException, IOException {
//		request.addHeader("Content-Type", "application/json; charset=utf-8");
		HttpClient client = new DefaultHttpClient();
		// initialize HTTP context for getting and setting cookies in request 
		HttpContext localContext = new BasicHttpContext(); 
		localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
		// execute request with our HTTP context
		HttpResponse response = client.execute(request, localContext);
		// perform response after request
		HttpEntity responseBody = response.getEntity();
		StatusLine responseStatus = response.getStatusLine();
		int statusCode = (responseStatus != null) ? responseStatus.getStatusCode() : 0;
		// form data for retrieving to the one who has called this service
		if (responseBody != null) {
			Bundle resultData = new Bundle();
			resultData.putString(RESULT_BODY, EntityUtils.toString(responseBody));
			receiver.send(statusCode, resultData);
		} else {
			receiver.send(statusCode, null);
		}
	}
	
}
