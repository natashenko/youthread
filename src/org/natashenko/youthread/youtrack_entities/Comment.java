package org.natashenko.youthread.youtrack_entities;

import java.util.HashMap;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class Comment {
	private HashMap<String, String> attrs = new HashMap<String, String>();
	
	public void setField(String fieldName, String value) {
		attrs.put(fieldName, value);
	}
	
	public static Comment parse(Node commentNode) {
		Comment comment = new Comment();
		NamedNodeMap xmlAttrs = commentNode.getAttributes();
		for (int i = 0; i < xmlAttrs.getLength(); ++i) {
			Node xmlAttr = xmlAttrs.item(i);
			comment.setField(xmlAttr.getNodeName(), xmlAttr.getNodeValue());
		}
		return comment;
	}

}
