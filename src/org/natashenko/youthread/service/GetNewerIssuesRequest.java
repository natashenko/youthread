package org.natashenko.youthread.service;

import java.util.Collections;
import java.util.List;

import org.natashenko.youthread.MainActivity;
import org.natashenko.youthread.Utils;
import org.natashenko.youthread.youtrack_entities.Issue;

import android.util.Log;


public class GetNewerIssuesRequest extends GetIssuesRequest {

    public GetNewerIssuesRequest(MainActivity activity) {
        super(activity);
    }

    @Override
    protected void addResultToActivity(List<Issue> resultItems) {
        Collections.sort(resultItems);
        activity.getViewedItems().addAll(0, resultItems);
//      Collections.sort(getList());
      activity.refreshAdapter();
      
      StringBuilder result = new StringBuilder();
      for (Issue issue : resultItems) {
          result.append(issue.getId() + " ");
      }
      Log.e(Utils.LOG_TAG, "result:" + result);
    }
    
    @Override
    protected String getFilterUpdatedValue(String date) {
        return "updated: " + date + " .. 9999-12-31 sort by: updated asc";
    }

    @Override
    protected Issue getExtremeItem() {
        return activity.getViewedItems().getFirst();
    }

}
