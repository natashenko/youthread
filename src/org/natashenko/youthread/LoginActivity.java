package org.natashenko.youthread;

import org.natashenko.youthread.service.LoginRequest;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends Activity implements Refreshable {
    private EditText hostView;
    private EditText userView;
    private EditText passwordView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(Utils.LOG_TAG, getClass().getSimpleName() + ": onCreate");
        // set progress bar possibility
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_login);
        // bind objects with their IDs
        hostView = (EditText) findViewById(R.id.host);
        userView = (EditText) findViewById(R.id.user);
        passwordView = (EditText) findViewById(R.id.password);
        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin(textView);
                    return true;
                }
                return false;
            }
        });
    }
    
    @Override
    protected void onPause() {
        Log.i(Utils.LOG_TAG, getLocalClassName() + ": onPause()");
        Utils.saveHost(getHost());
        Utils.saveUser(getUser());
        Utils.savePassword(getPassword());
        super.onPause();
    }
    
    @Override
    protected void onResume() {
        Log.i(Utils.LOG_TAG, getLocalClassName() + ": onResume()");
        hostView.setText(Utils.getHost());
        userView.setText(Utils.getUser());
        passwordView.setText(Utils.getPassword());
        super.onResume();
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.i(Utils.LOG_TAG, getLocalClassName() + ": onSaveInstanceState()");
        super.onSaveInstanceState(outState);
    }
    
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.i(Utils.LOG_TAG, getLocalClassName() + ": onRestoreInstanceState()");
        super.onRestoreInstanceState(savedInstanceState);
    }

    public String getHost() {
        String value = hostView.getText().toString();
        return URLUtil.guessUrl(value);
    }

    public String getUser() {
        return userView.getText().toString();
    }

    public String getPassword() {
        return passwordView.getText().toString();
    }

    /**
     * Attempts to sign in the account specified by the login form. If there are
     * form errors (invalid user, missing fields, etc.), the errors are
     * presented and no actual login attempt is made.
     */
    public void attemptLogin(View view) {
        hostView.setError(null);
        userView.setError(null);
        passwordView.setError(null);
        if (isFiledNotEmpty(hostView) && isFiledNotEmpty(userView) && isFiledNotEmpty(passwordView)) {
            LoginRequest request = new LoginRequest(this);
            request.execute();
        }
    }

    private boolean isFiledNotEmpty(EditText editText) {
        if (TextUtils.isEmpty(editText.getText().toString())) {
            editText.setError(getString(R.string.error_field_required));
            editText.requestFocus();
            return false;
        }
        return true;
    }

    // public void performLogin(BaseRequest prevRequest) {
    // save old request that was fail with UNAUTORIZED error
    // loginRequest.setNextRequest(prevRequest);
    // }

    @Override
    public void onRefreshStart() {
        setProgressBarIndeterminateVisibility(true);
    }

    @Override
    public void onRefreshComplete() {
        setProgressBarIndeterminateVisibility(false);
    }

    public void setPasswordError() {
        passwordView.setError(getString(R.string.error_forbidden));
        passwordView.requestFocus();
    }
}
