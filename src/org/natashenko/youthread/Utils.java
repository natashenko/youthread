package org.natashenko.youthread;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.URLUtil;

public class Utils {
	public static String LOG_TAG = "YouThreadLog";

	public static Context CONTEXT;
	
	public static String getResourse(int resId) {
		return CONTEXT.getString(resId);
	}
	
	public static SharedPreferences getPrefs() {
		String fileName = CONTEXT.getString(R.string.pref_file_name);
		return CONTEXT.getSharedPreferences(fileName, Context.MODE_PRIVATE);
	}
	
	public static String getHost() {
		String host = getPref(R.string.pref_host, R.string.default_host);
		host = URLUtil.guessUrl(host);
		return host;
	}
	
	public static String getUser() {
		return getPref(R.string.pref_user, R.string.default_user);
	}
	
	public static String getPassword() {
		return getPref(R.string.pref_password, R.string.default_password);
	}
	
	public static void saveHost(String value) {
		savePref(R.string.pref_host, value);
	}
	
	public static void saveUser(String value) {
		savePref(R.string.pref_user, value);
	}
	
	public static void savePassword(String value) {
		savePref(R.string.pref_password, value);
	}
	
	private static String getPref(int keyId, int defaultId) {
		SharedPreferences sharedPref = getPrefs();
		String prefKey = CONTEXT.getString(keyId);
		String prefDefault = CONTEXT.getString(defaultId);
		String value = sharedPref.getString(prefKey, prefDefault);
		if (TextUtils.isEmpty(value)) {
			return prefDefault;
		}
		return value;
	}
	
	private static void savePref(int keyId, String value) {
		SharedPreferences sharedPref = getPrefs();
		SharedPreferences.Editor editor = sharedPref.edit();
		String prefKey = CONTEXT.getString(keyId);
		editor.putString(prefKey, value);
		editor.commit();
	}
	
	public static void clearPrefs() {
		SharedPreferences sharedPref = Utils.getPrefs();
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.clear();
		editor.commit();
		Log.d(Utils.LOG_TAG, "host: " + Utils.getHost());
	}
}
